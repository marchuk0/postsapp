package com.example.postsapplication.ui.postslist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.postsapplication.R
import com.example.postsapplication.data.db.PostsDatabase
import com.example.postsapplication.data.db.entity.Post
import com.example.postsapplication.data.other.PostsItemAdapter
import com.example.postsapplication.data.repositories.PostsRepository
import kotlinx.android.synthetic.main.activity_posts.*

class PostsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_posts)

        val database = PostsDatabase(this);
        val repository = PostsRepository(database);
        val factory = PostsViewModelFactory(repository);

        val viewModel = ViewModelProviders.of(this, factory).get(PostsViewModel::class.java)
        viewModel.upsert(Post("PostName1", "PostText"))
        viewModel.upsert(Post("Postname2", "PostText\n multiline"))
//        viewModel.deleteAll();

        val adapter = PostsItemAdapter(listOf(), viewModel)

        rvPostsItems.layoutManager = LinearLayoutManager(this);
        rvPostsItems.adapter = adapter;
        viewModel.getAll().observe(
            this,
            Observer { it ->
                adapter.items = it
                adapter.notifyDataSetChanged()
            }
        )

    }
}
package com.example.postsapplication.ui.postslist

import androidx.lifecycle.ViewModel
import com.example.postsapplication.data.db.entity.Post
import com.example.postsapplication.data.repositories.PostsRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PostsViewModel(
    private val repository: PostsRepository
) : ViewModel() {

    fun upsert(post: Post) = CoroutineScope(Dispatchers.Main).launch {
        repository.upsert(post);
    }

    fun delete(post: Post) =  CoroutineScope(Dispatchers.Main).launch {
        repository.delete(post);
    }

    fun getAll() = repository.getAllPosts()

    fun deleteAll() = repository.deleteAll();



}

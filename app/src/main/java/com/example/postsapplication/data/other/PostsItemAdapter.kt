package com.example.postsapplication.data.other

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.postsapplication.R
import com.example.postsapplication.data.db.entity.Post
import com.example.postsapplication.ui.postslist.PostsViewModel
import kotlinx.android.synthetic.main.posts_item.view.*

class PostsItemAdapter(
    var items: List<Post>,
    private val ViewModel: PostsViewModel
): RecyclerView.Adapter<PostsItemAdapter.PostsViewHolder>() {

    inner class PostsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.posts_item, parent, false   );
        return PostsViewHolder(view);
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        val curPostsItem = items[position]

        holder.itemView.tvName.text = curPostsItem.name
        holder.itemView.tvText.text = curPostsItem.text
    }
}
package com.example.postsapplication.data.repositories

import com.example.postsapplication.data.db.PostsDatabase
import com.example.postsapplication.data.db.entity.Post

class PostsRepository(
    private val db: PostsDatabase
) {
    suspend fun upsert(post: Post) = db.getPostsDao().upsert(post);

    suspend fun delete(post: Post) = db.getPostsDao().delete(post);

    fun getAllPosts() = db.getPostsDao().getAll();

    fun deleteAll() = db.getPostsDao().deleteAll();
}
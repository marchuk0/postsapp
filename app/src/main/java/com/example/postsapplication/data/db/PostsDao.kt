package com.example.postsapplication.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.postsapplication.data.db.entity.Post

@Dao
interface PostsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(post: Post)

    @Delete
    suspend fun delete(post: Post)

    @Query("SELECT * FROM posts")
    fun getAll() : LiveData<List<Post>>

    @Query("DELETE  FROM posts")
    fun deleteAll()
}
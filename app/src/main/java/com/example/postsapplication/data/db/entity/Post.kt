package com.example.postsapplication.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "posts")
data class Post(
    @ColumnInfo(name="post_name")
    var name: String,
    @ColumnInfo(name="post_text")
    var text: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
}